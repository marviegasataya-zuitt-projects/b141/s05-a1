USE classicmodels;

-- 1. Return the customerName of the customers who are form the Philippines
SELECT customerName FROM `customers` WHERE country = 'Philippines';

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM `customers` WHERE customerName = 'La Rochelle Gifts';

-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP from `products` WHERE productName = "The Titanic";

-- 4. Return the firstaand last name of the employee whose email is 'jfirrelli@classicmodelcars.com'
SELECT firstName, lastName from `employees` WHERE email = 'jfirrelli@classicmodelcars.com';

-- 5. Return the names of customers who have no registered state 
SELECT customerName from `customers` WHERE state = null;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve;
SELECT firstName, lastName, email from `employees` WHERE lastName = 'Patterson' AND firstName = 'Steve';

-- 7. Return customer name, country and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit from `customers` WHERE  country != 'USA' AND creditLimit > 3000;

-- 8. Return the customer names of customers whose customer names don't have a 'a' in them
SELECT customerName from `customers` WHERE customerName NOT LIKE "%a%";

-- 9. Return the customer numbers of orders whose comments contain the string DHL
SELECT customers.customerNumber from `customers` JOIN orders ON 
customers.customerNumber = orders.customerNumber 
AND comments LIKE "%DHL%";

-- 10. Return the product lines whose text descroption mentions the phrase 'state of the art'
SELECT productLine FROM `productlines` WHERE textDescription LIKE "%state of the art%";

-- 11. Return the countries of customers whithout duplication
SELECT country FROM `customers` GROUP BY country;

-- 12. Return the statuses of orders whithout duplication
SELECT status FROM `orders` GROUP BY `status`;

-- 13. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country from `customers` 
WHERE country = 'USA' or country = 'France'  or country = 'Canada';

-- 14. Return the first name, last name and office's city of employees whose offices are Tokyo;
SELECT firstName, lastName from employees WHERE officeCode = 5;

-- 15. Return the customer names of customers who were served by the employee names "Leslie Thompson"
SELECT customerName from customers WHERE salesRepEmployeeNumber = 1166;

-- 16. Return the product names and customer name of products ordered by "Baane Mini Imports" //// last checked
SELECT customers.customerName, products.productName from customers 
    JOIN orders ON customers.customerNumber = orders.customerNumber
    JOIN orderdetails ON orders.orderNumber = orderDetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode
    WHERE customers.customerName = "Baane Mini Imports";

-- 17. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
    JOIN orders ON customers.customerNumber = orders.customerNumber
    JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber 
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE customers.country = offices.country
    GROUP BY customers.customerName


-- 18. Return the last names nad first names of employees being supervised by "Anthony Bow"
SELECT lastName, firstName from `employees` WHERE reportsTo = 1143;

-- 19. Return the product name and MSRP of the product with the highest MSRP
SELECT productName, MAX(MSRP) as `highest MSRP` from `products`;

-- 20. Return the number of customers in the UK
SELECT COUNT(*) as `Number of UK customers` FROM `customers` WHERE country = 'UK';

-- 21. Return the number of products per product line
SELECT productLine, COUNT(*) as `Number of Products` FROM products GROUP BY productLine;

-- 22. Return the number of customers served by every employee
SELECT CONCAT(lastName, ' ' , firstName) as `employee full name`, COUNT(*) as `Number of customers served` FROM `employees` 
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
GROUP BY salesRepEmployeeNumber;

-- 23. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 100
SELECT productName, quantityInStock FROM `products` WHERE productLine = 'Planes' AND quantityInStock < 1000;